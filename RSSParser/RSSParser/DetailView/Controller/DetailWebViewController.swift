//
//  DetailWebView.swift
//  RSSParser
//
//  Created by Andrei Rybak on 27.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit

internal class DetailWebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    internal var url: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = url {
            webView.loadRequest(NSURLRequest(URL: NSURL(string: url)!))
        }
    }
}
