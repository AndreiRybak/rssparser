//
//  StringExtensions.swift
//  RSSParser
//
//  Created by Andrei Rybak on 15.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation
import UIKit
import CoreData

//MARK: String Extension
extension String {
    
    internal func editedNewsDescription() -> String {
        var editedDescription: NSString = self
            
        if editedDescription.containsString("<img src") && editedDescription.containsString("<br") {
                
            var substringStart = editedDescription.rangeOfString("<img src")
            var substirngEnd = editedDescription.rangeOfString("/>")
            var range = NSRange(location: substringStart.location, length: (substirngEnd.location + substirngEnd.length) - substringStart.location)
            
            editedDescription = editedDescription.stringByReplacingCharactersInRange(range, withString: "")
            
            substringStart = editedDescription.rangeOfString("<br")
            substirngEnd = editedDescription.rangeOfString("/>")
            range = NSRange(location: substringStart.location, length: (substirngEnd.location + substirngEnd.length) - substringStart.location)
            
            editedDescription = editedDescription.stringByReplacingCharactersInRange(range, withString: "")
        }
        
        if editedDescription.containsString("\n\t\t") {
            editedDescription = editedDescription.stringByReplacingCharactersInRange(NSRange(location: 0, length: 3), withString: "")
        }
        return editedDescription as String
    }
    
    internal func editedNewsDateOrTitle() -> String {
        var editedDataOrTitle: NSString = self
        if editedDataOrTitle.containsString("\n\t\t"){
            editedDataOrTitle = editedDataOrTitle.stringByReplacingCharactersInRange(NSRange(location: 0, length: 3), withString: "")
        }
        if editedDataOrTitle.containsString("+0300") {
            editedDataOrTitle = editedDataOrTitle.stringByReplacingCharactersInRange(NSRange(location: editedDataOrTitle.length - 9, length: 9), withString: "")
        }
        return editedDataOrTitle as String
    }
    
    internal func editedNewsCategory() -> String {
        var editedDataOrTitle: NSString = self
        if editedDataOrTitle.containsString("TUT.BY: Новости ТУТ - "){
            editedDataOrTitle = editedDataOrTitle.stringByReplacingCharactersInRange(NSRange(location: 0, length: 24), withString: "")
        }
        return editedDataOrTitle as String
    }
}









