//
//  FavoriteNewsTableViewController.swift
//  RSSParser
//
//  Created by Andrei Rybak on 20.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation
import UIKit
import CoreData

internal class FavoriteNewsTableViewController: UITableViewController {
    
    private var newsList = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        loadFavoriteNews()
    }
    
    private func loadFavoriteNews() {
        
        let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let predicateForCategory = NSPredicate(format: "isFavorite == %@", true )
        let fetchRequest = NSFetchRequest(entityName: "News")
        fetchRequest.predicate = predicateForCategory
        
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            if let results = results as? [News] {
                newsList = results.sort({ $0.newsDate.compare($1.newsDate) == NSComparisonResult.OrderedDescending })
            }
        } catch let error as NSError {
            print("Error: \(error) " + "description \(error.localizedDescription)")
        }
        tableView.reloadData()
    }
    
    
    //MARK: UITableViewDataSource methods
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(FavoriteNewsTableViewCell.identifierForReuse()) as! FavoriteNewsTableViewCell
        
        if let news = newsList[indexPath.row] as? News {
            cell.setupCellForNews(news)
        }
        return cell
    }

    //Push Detail Web View
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if let news = newsList[indexPath.row] as? News {
            if let navigationController = navigationController {
                if let destinationVC = navigationController.storyboard?.instantiateViewControllerWithIdentifier("DetailWebViewControllerID") as? DetailWebViewController {
                    destinationVC.url = news.newsLink
                    navigationController.pushViewController(destinationVC, animated: true)
                }
            }
        }
    }
    
    
    
    //MARK: UITableViewDataSourceDelegate methods
    //Resizing cell depending on the content
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    //Delete news from favorite
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
       
        if let news = newsList[indexPath.row] as? News {
            if editingStyle == .Delete {
                news.isFavorite = false
                newsList.removeAtIndex(indexPath.row)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                tableView.reloadData()
                NSNotificationCenter.defaultCenter().postNotificationName("reloadData", object: nil)
            }
        }

        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Error: \(error) " + "description \(error.localizedDescription)")
        }
    }
    
}
