//
//  FavoriteNewsTableViewCell.swift
//  RSSParser
//
//  Created by Andrei Rybak on 28.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit

internal class FavoriteNewsTableViewCell: ReadedNewsTableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }
 
    override internal class func identifierForReuse() -> String {
        return "FavoriteNewsCell"
    }
}
