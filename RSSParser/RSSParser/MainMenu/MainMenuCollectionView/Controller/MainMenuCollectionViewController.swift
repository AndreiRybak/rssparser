//
//  MainMenuCollectionViewController.swift
//  RSSParser
//
//  Created by Andrei Rybak on 27.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit

private let reuseIdentifierCategoryCell = "CategoryCell"

internal class MainMenuCollectionViewController: UICollectionViewController, UIGestureRecognizerDelegate {
    
    private struct ConstantsURL {
        static let mainNews = "http://news.tut.by/rss/index.rss"
        static let politicalNews = "http://news.tut.by/rss/politics.rss"
        static let societyNews = "http://news.tut.by/rss/society.rss"
        static let sportNews = "http://news.tut.by/rss/sport.rss"
        static let financeNews = "http://news.tut.by/rss/finance.rss"
        static let autoNews = "http://news.tut.by/rss/auto.rss"
        static let posterNews = "http://news.tut.by/rss/afisha.rss"
    }
    
    private let categories = ["Главные новости","Политика","Общество","Спорт","Финансы","Авто","Афиша"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifierCategoryCell, forIndexPath: indexPath) as! CategoryCell
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor.blackColor().CGColor
        cell.categoryTitle.text = categories[indexPath.row]
        cell.categoryImage.image = UIImage(named: categories[indexPath.row])

        return cell
    }
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let destinationViewController = navigationController?.storyboard?.instantiateViewControllerWithIdentifier("MainScreenID") as? MainScreenTableViewController {
            navigationController?.pushViewController(destinationViewController, animated: true)
            switch categories[indexPath.row] {
            case "Главные новости":
                destinationViewController.loadNews(ConstantsURL.mainNews, category: categories[indexPath.row])
            case "Политика":
                destinationViewController.loadNews(ConstantsURL.politicalNews, category: categories[indexPath.row])
            case "Общество":
                destinationViewController.loadNews(ConstantsURL.societyNews, category: categories[indexPath.row])
            case "Спорт":
                destinationViewController.loadNews(ConstantsURL.sportNews, category: categories[indexPath.row])
            case "Финансы":
                destinationViewController.loadNews(ConstantsURL.financeNews, category: categories[indexPath.row])
            case "Авто":
                destinationViewController.loadNews(ConstantsURL.autoNews, category: categories[indexPath.row])
            case "Афиша":
                destinationViewController.loadNews(ConstantsURL.posterNews, category: categories[indexPath.row])
            default:
                break
            }
        }
    }
    
    override func collectionView(collectionView: UICollectionView, didHighlightItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CategoryCell
        let imageName = categories[indexPath.row] + "Highlighted"
        cell.categoryImage.image = UIImage(named: imageName)
    }
    

    override func collectionView(collectionView: UICollectionView, didUnhighlightItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CategoryCell
        cell.categoryImage.image = UIImage(named: categories[indexPath.row])
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
   
        if indexPath.row == 0 {
            return CGSize(width: collectionView.frame.width, height: 100)
        }
        return CGSize(width: (collectionView.frame.width / 2) - 5, height: 100)
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        collectionView?.reloadData()
    }

}
