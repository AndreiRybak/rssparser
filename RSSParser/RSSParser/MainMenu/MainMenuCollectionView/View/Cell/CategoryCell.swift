//
//  CategoryCell.swift
//  RSSParser
//
//  Created by Andrei Rybak on 27.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit

internal class CategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
}
