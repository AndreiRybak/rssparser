//
//  MainScreenTableViewController.swift
//  RSSParser
//
//  Created by Andrei Rybak on 14.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//
import Foundation
import UIKit
import CoreData
import Kingfisher

internal class MainScreenTableViewController: UITableViewController {
    
    private let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    private var newsList = [NSManagedObject]()
    private var category: Category?
    private let session = SessionManager.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(reloadData), name:"reloadData", object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        reloadData()
    }
    
    internal func loadNews(rssURL: String, category: String) {
        session.provideNews(rssURL){ [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.navigationItem.title = NSLocalizedString(category, comment: "")
            
            //Fetching news ojbects from core data
            let predicateForCategory = NSPredicate(format: "name == %@", category )
            let fetchRequest = NSFetchRequest(entityName: "Category")
            fetchRequest.predicate = predicateForCategory
            do {
                let results = try strongSelf.managedContext.executeFetchRequest(fetchRequest)
                if let category = results[0] as? Category {
                    strongSelf.category = category
                }
                let newsList = strongSelf.category?.news?.allObjects as! [News]
                strongSelf.newsList = newsList.sort({ $0.newsDate.compare($1.newsDate) == NSComparisonResult.OrderedDescending })
            } catch let error as NSError {
                print("Error: \(error) " + "description \(error.localizedDescription)")
            }
            //Reload tableView in main queue
            NSOperationQueue.mainQueue().addOperationWithBlock() {
                strongSelf.tableView.reloadData()
            }
        }
    }

    @objc private func reloadData() {
        tableView.reloadData()
    }
    
    @IBAction func addNewsToFavorite(sender: UIButton) {
        let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        if let news = newsList[sender.tag] as? News {
            news.isFavorite = true
            sender.enabled = false
            sender.setImage(UIImage(named: "isFavorite"), forState: .Disabled)
            tableView.reloadData()
        }
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Error: \(error) " + "description \(error.localizedDescription)")
        }
    }
    
    //MARK: UITableViewDataSource methods
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList.count
    }
    
    
    //Setup cells & download news images
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(MainScreenTableViewCell.identifierForReuse()) as! MainScreenTableViewCell
        cell.favoriteButton.tag = indexPath.row
        
        if let news = newsList[indexPath.row] as? News {
            cell.setCellBackgroundColor(news)
            
            cell.favoriteButton.enabled = Int(news.isFavorite) > 0 ? false : true
            cell.setupCellForNews(news)
            
            if let _ = news.newsImage {
                cell.newsImage.image = UIImage(data: news.newsImage!)
            } else if (!tableView.dragging && !tableView.decelerating && news.newsImage == nil) {
                cell.newsImage.kf_setImageWithURL(NSURL(string: news.newsImageURL), placeholderImage: UIImage(named:"placeholderImage"), optionsInfo: nil, progressBlock: nil) {
                    [weak self] (image, error, cacheType, imageURL) in
                    guard let strongSelf = self else { return }
                    
                    if image != nil {
                        news.newsImage = UIImageJPEGRepresentation(image!, 0)
                    }

                    do {
                        try strongSelf.managedContext.save()
                    } catch let error as NSError {
                        print("Error: \(error) " + "description \(error.localizedDescription)")
                    }
                }
            }
        }
        return cell
    }
    

    //Push Detail Web View
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        if let news = newsList[indexPath.row] as? News {
            if let navigationController = navigationController {
                if let destinationVC = navigationController.storyboard?.instantiateViewControllerWithIdentifier("DetailWebViewControllerID") as? DetailWebViewController {
                    news.isVisited = true
                    news.viewDate = NSDate()
                    destinationVC.url = news.newsLink
                    navigationController.pushViewController(destinationVC, animated: true)
                }
            }
        }
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Error: \(error) " + "description \(error.localizedDescription)")
        }
    }

    
    
    //MARK: UIScrollViewDelegate methods
    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
       reloadData()
    }
    
    override func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
       reloadData() 
    }
    
    //MARK: UITableViewDataSourceDelegate methods
    //Resizing cell depending on the content
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

