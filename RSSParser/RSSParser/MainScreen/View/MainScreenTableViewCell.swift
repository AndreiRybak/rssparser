//
//  MainScreenTableViewCell.swift
//  RSSParser
//
//  Created by Andrei Rybak on 14.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//
import Foundation
import UIKit

internal class MainScreenTableViewCell: UITableViewCell {
    
    private let dateFormatter = NSDateFormatter()

    @IBOutlet weak var favoriteButton: UIButton!
    
    @IBOutlet private weak var newsTitle: UILabel!
    @IBOutlet private weak var newsDescription: UILabel!
    @IBOutlet internal weak var newsImage: UIImageView!
    @IBOutlet private weak var newsDate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        dateFormatter.locale = NSLocale(localeIdentifier: "US_en")
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        contentView.backgroundColor = UIColor.whiteColor()
        newsTitle.text = ""
        newsDescription.text = ""
        newsImage.image = UIImage(named: "placeholderImage")
        newsDate.text = ""
        
    }
    
    internal static func identifierForReuse() -> String {
        return "MainScreenCell"
    }
    
    internal func setCellBackgroundColor(news: News) {
        if Int(news.isVisited) > 0 {
            //gray color
            contentView.backgroundColor = UIColor(red: 225/255, green: 230/255, blue: 232/255, alpha: 1)
        }
    }
    
    internal func setupCellForNews(news: News) {
        newsTitle.text = news.newsTitle
        newsDescription.text = news.newsDescription
        newsDate.text = dateFormatter.stringFromDate(news.newsDate)

        favoriteButton.imageEdgeInsets = UIEdgeInsets(top: 18, left: 0, bottom: 0, right: 0)
        if Int(news.isFavorite) > 0 {
            
            favoriteButton.setImage(UIImage(named: "isFavorite"), forState: .Disabled)
        } else {
            favoriteButton.setImage(UIImage(named: "notFavorite"), forState: .Normal)
        }

    }
    
}