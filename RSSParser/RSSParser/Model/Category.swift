//
//  Category.swift
//  RSSParser
//
//  Created by Andrei Rybak on 04.10.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation
import CoreData

internal class Category: NSManagedObject {

    @NSManaged var name: String?
    @NSManaged var news: NSSet?

}
