//
//  News.swift
//  RSSParser
//
//  Created by Andrei Rybak on 14.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation
import CoreData

internal class News: NSManagedObject {
    
    @NSManaged var newsCategory: String
    @NSManaged var newsTitle: String
    @NSManaged var newsDescription: String
    @NSManaged var newsDate: NSDate
    @NSManaged var newsLink: String
    
    @NSManaged var newsImageURL: String
    @NSManaged var newsImage: NSData?

    @NSManaged var isFavorite: NSNumber
    @NSManaged var isVisited: NSNumber
    @NSManaged var viewDate: NSDate?
    
    @NSManaged var category: NSManagedObject?
    
}

