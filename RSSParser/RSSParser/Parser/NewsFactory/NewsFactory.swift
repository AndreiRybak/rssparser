//
//  NewsFactory.swift
//  RSSParser
//
//  Created by Andrei Rybak on 14.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation
import UIKit
import CoreData

internal class NewsFactory: NewsFactoryProtocol {
    
    private let privateManagedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).privateManagedObjectContext
    
    private var newsObjects = [News]()
    private var categoryObjects = [Category]()
    
    internal func createNewsList(newsDictionary: [Dictionary<String,String>]) {
        var newsDate: NSDate?
        var newsDateString = ""
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm"
        dateFormatter.locale = NSLocale(localeIdentifier: "US_en")
        
        var newsCategory = ""
        var newsTitle = ""
        var newsDescription = ""
        var newsImageURL = ""
        var newsLink = ""
        
        
        fetchCurrentObjects()

        //Parse data dictionary by keys
        for news in newsDictionary {
            
            newsTitle = news["newsTitle"] != nil ? (news["newsTitle"]!).editedNewsDateOrTitle() : newsTitle
            newsDescription = news["newsDescription"] != nil ? (news["newsDescription"]!).editedNewsDescription() : newsDescription
            newsImageURL = news["newsImageURL"] != nil ? news["newsImageURL"]! : newsImageURL
            newsLink = news["newsLink"] != nil ? (news["newsLink"]!).editedNewsDateOrTitle() : newsLink
            newsCategory = news["newsCategory"] != nil ? (news["newsCategory"]!).editedNewsCategory() : newsCategory
            newsDateString = news["newsDate"] != nil ? (news["newsDate"]!).editedNewsDateOrTitle() : newsDateString
        
            if let date = dateFormatter.dateFromString(newsDateString) {
                newsDate = date
            }
            
            saveNewsObject(newsTitle, newsDescription: newsDescription, newsImageURL: newsImageURL, newsLink: newsLink, newsDate: newsDate!, newsCategory: newsCategory)
            saveCategoryObject(newsCategory)
            newsToCategory(newsTitle, newsDate: newsDate!, categoryName: newsCategory)
        }
        
        saveContext()
    }
    
    private func fetchCurrentObjects() {
        
        let newsFetchRequest = NSFetchRequest(entityName: "News")
        let categoryFetchRequest = NSFetchRequest(entityName: "Category")
        
        newsObjects.removeAll()
        categoryObjects.removeAll()
        
        //Fetch news
        do {
            newsObjects = try privateManagedContext.executeFetchRequest(newsFetchRequest) as! [News]
        } catch let error as NSError {
            print("Error: \(error) " + "description \(error.localizedDescription)")
        }

        //Fetch categories
        do {
            categoryObjects = try privateManagedContext.executeFetchRequest(categoryFetchRequest) as! [Category]
        } catch let error as NSError {
            print("Error: \(error) " + "description \(error.localizedDescription)")
        }
    }
    
    private func saveNewsObject(newsTitle: String, newsDescription: String, newsImageURL: String, newsLink: String, newsDate: NSDate, newsCategory: String) {
        
        let newsEntity = NSEntityDescription.entityForName("News", inManagedObjectContext: privateManagedContext)
        
        //Check for the presence of object in array
        var flag = false
        for news in newsObjects {
            if news.newsDate == newsDate && news.newsTitle == newsTitle {
                flag = true
                break
            } else {
                flag = false
            }
        }
        
        if !flag {
            //Create new news object
            if let newsObject = NSManagedObject(entity: newsEntity!, insertIntoManagedObjectContext: privateManagedContext) as? News {
                newsObject.newsTitle = newsTitle
                newsObject.newsDescription = newsDescription
                newsObject.newsDate = newsDate
                newsObject.newsLink = newsLink
                newsObject.newsImageURL = newsImageURL
                newsObject.newsCategory = newsCategory

                newsObjects.append(newsObject)
            }
        }
    }
    
    private func saveCategoryObject(name: String) {
        
        let categoryEntity = NSEntityDescription.entityForName("Category", inManagedObjectContext: privateManagedContext)
        
        var flag = false
        for category in categoryObjects {
            if category.name == name {
                flag = true
                break
            } else {
                flag = false
            }
        }
        
        if !flag {
            if let categoryObject = NSManagedObject(entity: categoryEntity!, insertIntoManagedObjectContext: privateManagedContext) as? Category {
                categoryObject.name = name
                
                categoryObjects.append(categoryObject)
            }
        }
    }
    
    private func newsToCategory(newsTitle: String, newsDate: NSDate, categoryName: String) {
        for category in categoryObjects {
            if category.name == categoryName {
                for news in newsObjects {
                    if news.newsTitle == newsTitle && news.newsDate == newsDate {
                        category.news?.setByAddingObject(news)
                        news.category = category
                    }
                }
            }
        }
    }
    
    
    private func saveContext() {
    
        do {
            try privateManagedContext.save()
            print("save")
        } catch let error as NSError {
            print("Error: \(error) " + "description \(error.localizedDescription)")
        }
    }
    
    
}