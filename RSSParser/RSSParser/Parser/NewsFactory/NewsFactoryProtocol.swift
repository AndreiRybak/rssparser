//
//  NewsFactoryProtocol.swift
//  RSSParser
//
//  Created by Andrei Rybak on 15.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation

protocol NewsFactoryProtocol {
    func createNewsList(newsDictionary: [Dictionary<String,String>])
}