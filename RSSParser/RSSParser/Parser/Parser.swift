//
//  Parser.swift
//  RSSParser
//
//  Created by Andrei Rybak on 14.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation

internal class Parser: NSObject {
    
    private var insideAnItem = false
    
    private var currentElement = ""
    private var currenAttributes = [String:String]()
    
    private var newsCategory = ""
    private var newsTitle = ""
    private var newsDescription = ""
    private var newsImageURL = ""
    private var newsDate = ""
    private var newsLink = ""
    
    private var currentNews = [String: String]()
    private var parsedNews = [Dictionary<String, String>]()
    
    override init() {
        super.init()
    }
    
    internal func parseData(data: NSData) -> [Dictionary<String,String>] {
        let parser = NSXMLParser(data: data)
        parser.delegate = self
        parser.parse()
        return parsedNews
    }
}

//MARK: NSXMLParserDelegate methods
extension Parser: NSXMLParserDelegate {
    
    internal func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        
        currentElement = elementName
        currenAttributes = attributeDict
        
        if elementName == "item" {
            insideAnItem = true
        }
        
        if elementName == "enclosure" {
            if let photoUrl = currenAttributes["url"] {
                newsImageURL = photoUrl
            }
        }
    }
    
    internal func parser(parser: NSXMLParser, foundCharacters string: String) {
        if !insideAnItem && currentElement == "title" {
            newsCategory += string
        }
        
        guard insideAnItem else { return }
        
        switch currentElement {
        case "title":
            newsTitle += string
        case "description":
            newsDescription += string
        case "guid":
            newsLink += string
        case "pubDate":
            newsDate += string
        default:
            break
        }
    }
    
    internal func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if !insideAnItem && currentElement == "title" {
            currentNews["newsCategory"] = newsCategory
            newsCategory = ""
        }
        
        guard insideAnItem else { return }
        
        switch elementName {
        case "title":
            currentNews["newsTitle"] = newsTitle
            newsTitle = ""
        case "description":
            currentNews["newsDescription"] = newsDescription
            newsDescription = ""
        case "enclosure":
            currentNews["newsImageURL"] = newsImageURL
            newsImageURL = ""
        case "guid":
            currentNews["newsLink"] = newsLink
            newsLink = ""
        case "pubDate":
            currentNews["newsDate"] = newsDate
            newsDate = ""
        case "item":
            insideAnItem = false
            parsedNews.append(currentNews)
        default:
            break
        }
    }
}