//
//  SessionManager.swift
//  RSSParser
//
//  Created by Andrei Rybak on 14.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import Foundation

internal class SessionManager {
    
    static let sharedInstance = SessionManager()
    
    private let session: NSURLSession
    
    private let parser = Parser()
    private let newsFactory: NewsFactoryProtocol?
    
    private init() {
        session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
        newsFactory = NewsFactory()
    }
    
    //Provide data from URL.
    //Call method for parse this data & save them in core data.
    internal func provideNews(url: String, callbackBlock: () -> Void) {
        session.dataTaskWithURL(NSURL(string: url)!) { [weak self] (data, response, error) in
            guard let strongSelf = self else { return }
            
            if let error = error {
                print("Error: \(error) " + "description \(error.localizedDescription)")
                return
            }
            
            if let data = data {
                var parsedData = [Dictionary<String, String>]()
                parsedData = strongSelf.parser.parseData(data)
                if strongSelf.newsFactory != nil {
                    strongSelf.newsFactory!.createNewsList(parsedData)
                }
            }
            callbackBlock()
            
        }.resume()
    }

}