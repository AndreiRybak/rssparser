//
//  ReadedNewsTableViewController.swift
//  RSSParser
//
//  Created by Andrei Rybak on 20.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit
import Foundation
import CoreData

internal class ReadedNewsTableViewController: UITableViewController {
    
    private var newsSourceList = [NSManagedObject]()
    private var stateList = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sortButton = UIBarButtonItem(title: NSLocalizedString("Search", comment: ""), style: UIBarButtonItemStyle(rawValue: 0)!, target: self, action: #selector(downloadSheet))
        navigationItem.setRightBarButtonItem(sortButton, animated: true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        loadReadedNews()
    }
    
    private func loadReadedNews() {
        let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let predicateForCategory = NSPredicate(format: "isVisited == %@", true )
        let fetchRequest = NSFetchRequest(entityName: "News")
        fetchRequest.predicate = predicateForCategory
        
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            if let results = results as? [News] {
                newsSourceList = results.sort({ $0.viewDate!.compare($1.viewDate!) == NSComparisonResult.OrderedDescending })
                stateList = results.sort({ $0.viewDate!.compare($1.viewDate!) == NSComparisonResult.OrderedDescending })
            }
        } catch let error as NSError {
            print("Error: \(error) " + "description \(error.localizedDescription)")
        }
        tableView.reloadData()
    }
    
    
    //MARK: UITableViewDataSource methods
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stateList.count
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(ReadedNewsTableViewCell.identifierForReuse()) as! ReadedNewsTableViewCell
        
        if let news = stateList[indexPath.row] as? News {
            cell.setupCellForNews(news)
        }
        return cell
    }
    
    //Push Detail Web View
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if let news = stateList[indexPath.row] as? News {
            if let navigationController = navigationController {
                if let destinationVC = navigationController.storyboard?.instantiateViewControllerWithIdentifier("DetailWebViewControllerID") as? DetailWebViewController {
                    destinationVC.url = news.newsLink
                    navigationController.pushViewController(destinationVC, animated: true)
                }
            }
        }
    }
    
    
    //MARK: UITableViewDataSourceDelegate methods
    //Resizing cell depending on the content
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}

extension ReadedNewsTableViewController: UIActionSheetDelegate{
    
    @objc internal func downloadSheet() {
        
        let alertController = UIAlertController(title: "", message: nil, preferredStyle: .Alert)
        
        let cancelButton: UIAlertAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .Cancel) { action -> Void in }
        alertController.addAction(cancelButton)
        
        
        let searchNews: UIAlertAction = UIAlertAction(title: NSLocalizedString("Search", comment: ""), style: .Default)
        { [weak self] action -> Void in
            
            guard let strongSelf = self else { return }
            
            let textField = alertController.textFields![0]
            
            if let newsSourceList = strongSelf.newsSourceList as? [News] {
                strongSelf.stateList = newsSourceList.filter() { (news) -> Bool in
                    return textField.text == "" || news.newsTitle.lowercaseString.rangeOfString(textField.text!) != nil
                }

            }
            
            strongSelf.tableView.reloadData()
        }
        
        alertController.addAction(searchNews)
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Find"
        }
        
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    
}

