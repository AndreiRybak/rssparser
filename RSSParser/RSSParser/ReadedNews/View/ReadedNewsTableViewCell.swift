//
//  ReadedNewsTableViewCell.swift
//  RSSParser
//
//  Created by Andrei Rybak on 20.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import UIKit
import Foundation

internal class ReadedNewsTableViewCell: UITableViewCell {
    
    private let dateFormatter = NSDateFormatter()
    
    @IBOutlet private weak var newsTitle: UILabel!
    @IBOutlet private weak var newsDescription: UILabel!
    @IBOutlet private weak var newsDate: UILabel!
    @IBOutlet private weak var newsImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        dateFormatter.locale = NSLocale(localeIdentifier: "US_en")
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        newsTitle.text = ""
        newsDescription.text = ""
        newsImage.image = nil
        newsDate.text = ""
    }
    
    internal func setupCellForNews(news: News) {
        newsTitle.text = news.newsTitle
        newsDescription.text = news.newsDescription
        newsDate.text = dateFormatter.stringFromDate(news.newsDate)
        if news.newsImage != nil {
           newsImage.image = UIImage(data: news.newsImage!)
        }
    }
    
    internal class func identifierForReuse() -> String {
        return "ReadedNewsCell"
    }
}
