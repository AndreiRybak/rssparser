//
//  RSSParserTests.swift
//  RSSParserTests
//
//  Created by Andrei Rybak on 30.09.16.
//  Copyright © 2016 Andrei Rybak. All rights reserved.
//

import XCTest

class RSSParserTests: XCTestCase {
    
    var parser: Parser!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        parser = Parser()
    }
    
    func testParsing() {
        let resultDictionary = ["newsDate":"Mon, 03 Oct 2016 01:00:00 +0300","newsCategory":"\n\tГлавные новости", "newsLink":"http://news1", "newsDescription":"News description 1", "newsTitle":"News title 1", "newsImageURL":"http://image1.jpg"]
        
        let data = NSFileManager.defaultManager().contentsAtPath("/Users/Andrei_Rybak/Developer/rssparser/RSSParser/RSSParserTests/ParserTests/index.rss")
        let parsedData = parser.parseData(data!)
        
        var flag = false
        
        for key in parsedData[0].keys {
            if resultDictionary[key] == parsedData[0][key] {
                flag = true
            } else {
                flag = false
                break
            }
        }
        XCTAssert(flag)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }

}
